#include "bot.h"

bot::bot(int b[9][9])
{
	_curr = new board(b);
}

void bot::solveSoduko()
{
	checkRowCol();
	_curr->printBoard();
	if (!_curr->checkComplete())
	{
		solveSoduko();
	}
}

void bot::checkRowCol()
{
	std::vector<int>::iterator k;
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			std::cout<< i << "     " << j << std::endl;
			if (_curr->getSquare(i, j).getCurr() == -1)
			{
				for (int k = 0, l = 0; l != _curr->getSquare(i, j).getNums().size(); l++, k++)
				{
					if (!numPerSq(_curr->getSquare(i, j), _curr->getSquare(i, j).getNums()[k]))
					{
						
						if (_curr->getSquare(i, j).removeNum(_curr->getSquare(i, j).getNums()[k]))
						{
							break;
						}
						else
						{
							k -= 1;
						}
					}
				}
			}
		}
	}
}

bool bot::numPerSq(Square th, int check)
{
	std::tuple<int, int> pos = th.getPos();
	int gr = th.getGrid();
	int c = gr % 3;
	int r = gr / 3;
	for (int i = 0; i < 9; i++)
	{
		if (i != std::get<1>(pos))
		{
			//std::cout << _curr->getSquare(i, std::get<0>(pos)).getCurr() << "    1" << std::endl;
			if (_curr->getSquare(i, std::get<0>(pos)).getCurr() == check)
			{
				return false;
			}
		}
	}
	for (int i = 0; i < 9; i++)
	{
		if (i != std::get<0>(pos))
		{
			//std::cout << _curr->getSquare(std::get<1>(pos), i).getCurr() << "    2" << std::endl;
			if (_curr->getSquare(std::get<1>(pos), i).getCurr() == check)
			{
				return false;
			}
		}
	}

	for (int i = c; i < c + 3; i++)
	{
		for (int j = r; j < r + 3; j++)
		{
			if (!(i == std::get<1>(pos) && j == std::get<0>(pos)))
			{
				//std::cout << _curr->getSquare(i, j).getCurr() << "    3" << std::endl;
				if (_curr->getSquare(i, j).getCurr() == _curr->getSquare(std::get<1>(pos), std::get<0>(pos)).getCurr())
				{
					return false;
				}
			}
		}
	}
	
	return true;
}

