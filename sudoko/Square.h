#pragma once

#include <vector>
#include<tuple>
#include <iterator>
#include <algorithm>
#include<iostream>
class Square
{
public:
	Square(std::tuple<int, int>, int val);
	std::vector<int> getNums();
	void addNum(int);
	int getCurr();
	void setCurr(int);
	bool removeNum(int);
	int getGrid();
	std::tuple<int, int> getPos();
private:
	std::vector<int> _possibleNums;
	std::tuple<int, int> _pos;
	int _curr;
	int _grid;
};