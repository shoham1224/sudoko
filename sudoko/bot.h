#pragma once
#include "board.h"

class bot
{
public:
	bot(int[9][9]);
	void solveSoduko();
private:
	void checkRowCol();
	bool numPerSq(Square, int);

	board* _curr;
};