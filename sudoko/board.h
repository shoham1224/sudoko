#pragma once
#include "Square.h"

class board
{
public:
	board(int b[9][9]);
	void printBoard();
	Square getSquare(int, int);
	bool checkComplete();
private:
	Square* _newBoard[9][9];
	
};