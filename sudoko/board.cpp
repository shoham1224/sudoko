#include "board.h"

board::board(int b[9][9] )
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			_newBoard[i][j] = new Square(std::make_tuple(i, j), b[i][j]);
			if (b[i][j] == -1)
			{
				for (int k = 1; k <= 9; k++)
				{
					_newBoard[i][j]->addNum(k);
				}
			}
		}
	}
}

void board::printBoard()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			std::cout<< _newBoard[i][j]->getCurr() << " ";
		}
		std::cout << std::endl;
	}
}

Square board::getSquare(int x, int y)
{
	return *(_newBoard[x][y]);
}

bool board::checkComplete()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			if (_newBoard[i][j]->getCurr() == -1)
			{
				return false;
			}
		}
	}
	return true;
}
