#include "Square.h"

Square::Square(std::tuple<int, int> pos, int val)
{
	_pos = pos;
	int r, c;
	r = (std::get<0>(pos)) / 3;
	c = (std::get<1>(pos)) / 3;
	_grid = r * 3 + c;
	_curr = val;

}

std::vector<int> Square::getNums()
{
	return _possibleNums;
}

void Square::addNum(int num)
{
	_possibleNums.push_back(num);
}

int Square::getCurr()
{
	//std::cout << _curr << std::endl;
	return _curr;
}

void Square::setCurr(int curr)
{
	_curr = curr;
}

bool Square::removeNum(int num)
{
	std::vector<int>::iterator it;

	it = std::find(_possibleNums.begin(), _possibleNums.end(), num);
	if (it != _possibleNums.end())
	{
		_possibleNums.erase(it);
	}
	if (_possibleNums.size() == 0)
	{
		std::cout<< "EXCEPTION: NO NUMS POSSIBLE, BOT FAILED" << std::endl;
	}
	else if (_possibleNums.size() == 1)
	{
		_curr = _possibleNums[0];
	}
	return (_possibleNums.size() == 1);

}

int Square::getGrid()
{
	return _grid;
}

std::tuple<int, int> Square::getPos()
{
	return _pos;
}
